class Student {
    constructor(name,email,grades) {
        this.name = name;
        this.email = email;
        this.average = undefined;
        this.isPassed = undefined;
        this.isPassedWithHonors = undefined;
        if(grades.some(grade => typeof grade !== 'number')){
            this.grades = undefined
        } else{
            this.grades = grades;
        }
    }

    login(){

        console.log(`${this.email} has logged in`);
        return this;
    }

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    }

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => accumulator += num)
        this.average = Math.round(sum/4);
        return this;
    }

    willPass() {
        this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {

       this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
       return this;
    }
}

class Section {
    constructor(name){

        this.name = name;
        this.students = [];
        this.honorStudents = undefined;
        this.passedStudents = undefined;
        this.sectionAve = undefined;

    }

    addStudent(name,email,grades){
        this.students.push(new Student(name,email,grades));
        return this;
    }

    countHonorStudents(){
        let count = 0;

        this.students.forEach(student => {
            // console.log(student);

            // console.log(student.willPassWithHonors().isPassedWithHonors);
            student.willPassWithHonors();

            if(student.isPassedWithHonors){
                count++
            };
        })

        this.honorStudents = count;
        return this;
    }

    getNumberOfStudents(){
        return this.students.length;
    }

    countPassedStudents(){
        let count = 0;

        this.students.forEach(student => {
            student.willPass();
            if(student.isPassed){
                count++
            };
        })


        this.passedStudents = count;
        return this;
    }

    computeSectionAve(){
        let countAve = [];

        this.students.forEach(student => {
            countAve.push(student.computeAve().average);
        })
        let sum = countAve.reduce((accum,num) => accum += num)
        this.sectionAve = Math.round(sum/countAve.length);
        return this;  
    }

}

let section1A = new Section("Section1A");

// console.log(section1A)

section1A.addStudent("Joy", "joy@mail.com", [89,91,92,88]);
section1A.addStudent("Jeff", "jeff@mail.com", [81,80,82,78]);
section1A.addStudent("John", "john@mail.com", [91,90,92,96]);
section1A.addStudent("Jack", "jack@mail.com", [95,92,92,93]);

// console.log(section1A)

// console.log(section1A.students[2].computeAve().average);

// console.log("did jeff pass?",section1A.students[1].willPass().isPassed)

// section1A.countHonorStudents();
// console.log(section1A);

//******** ACTIVITY **********

section1A.addStudent("Alex", "alex@mail.com", [84,85,85,86]);
console.log(section1A)